def base2(x):
    cont=0
    for i in x:
        if (i=="0" or i=="1"):
            cont+=1
    if(cont ==len(x)):
        return 1
    else:
        return 0
    
def base8(x):
    cont=0
    for i in x:
        if (i=="0" or i=="1" or i=="2" or i=="3" or i=="4" or i=="5" or i=="6" or i=="7"):
            cont+=1
    if(cont ==len(x)):
        return 1
    else:
        return 0
    
def base10(x):
    cont=0
    for i in x:
        if (i=="0" or i=="1" or i=="2" or i=="3" or i=="4" or i=="5" or i=="6" or i=="7" or i=="8" or i=="9"):
            cont+=1
    if(cont ==len(x)):
        return 1
    else:
        return 0
    
def base16(x):
    cont=0
    for i in x:
        if (i=="0" or i=="1" or i=="2" or i=="3" or i=="4" or i=="5" or i=="6" or i=="7"or i=="8" or i=="9" or i=="A" or i=="B" or i=="C" or i=="D" or i=="E" or i=="F" or i=="a" or i=="b" or i=="c" or i=="d" or i=="e" or i=="f"):
            cont+=1
    if(cont ==len(x)):
        return 1
    else:
        return 0