# Ejercicios Simples :

## Miguel Gozález 20172020023<br/>
[3. Escribir un programa que sume, reste, multiplique y divida dos números leídos desde el teclado.](https://gitlab.com/magordini/modelos-codificacion/-/blob/master/Ejercicio3.py)<br/>
[5. Escribir un programa que calcule el área de un triángulo, capturando los valores de base y altura. área del triángulo = (base * altura)/2](https://gitlab.com/magordini/modelos-codificacion/-/blob/master/Ejercicio5.py)<br/>
[19.Escribir un programa que detecte si un número leído desde el teclado es mayor o menor que 100.](https://gitlab.com/magordini/modelos-codificacion/-/blob/master/Ejercicio19.py)<br/>
[21.Escribir un programa que dado un número del 1 a 7 escriba el correspondiente nombre del día de la semana.](https://gitlab.com/magordini/modelos-codificacion/-/blob/master/Ejercicio21.py)<br/>
[35.Escribir un programa que calcule el factorial de un número.](https://gitlab.com/magordini/modelos-codificacion/-/blob/master/Ejercicio35.py)<br/>
[37.Escribir un programa que calcule la media de números introducidos por el teclado hasta que el número ingresado sea cero.](https://gitlab.com/magordini/modelos-codificacion/-/blob/master/Ejercicio37.py)<br/>
[43.Escribir un programa que llene una lista con los veinte primeros números pares y calcule su suma.](https://gitlab.com/magordini/modelos-codificacion/-/blob/master/Ejercicio43.py)<br/>
[44.Escribir un programa que solicite cinco números, los almacene en una lista y luego calcule la media aritmética de esos números. ](https://gitlab.com/magordini/modelos-codificacion/-/blob/master/Ejercicio44.py)<br/>
[56.Escribir un programa que elimine los blancos de una cadena de caracteres. La cadena original y la transformada deben almacenarse de forma independiente.](https://gitlab.com/magordini/modelos-codificacion/-/blob/master/Ejercicio56.py)<br/>
[61.Escribir un programa que lea una frase introducida desde el teclado y la escriba al revés.](https://gitlab.com/magordini/modelos-codificacion/-/blob/master/Ejercicio61.py)<br/>
[64.Escribir un programa que incremente un número usando una función.](https://gitlab.com/magordini/modelos-codificacion/-/blob/master/Ejercicio64.py)<br/>
[65.Escribir un programa que calcule la potencia usando una función propia (no debe usar la funciones nativas del lenguaje u operadores que realicen esta operación).](https://gitlab.com/magordini/modelos-codificacion/-/blob/master/Ejercicio65.py)<br/>
[76.Escriba un programa que calcule el factorial de un numero usando recursividad.](https://gitlab.com/magordini/modelos-codificacion/-/blob/master/Ejercicio76.py)<br/>
[79.Escribir una función recursiva que halle la suma de los primeros "n" números naturales.](https://gitlab.com/magordini/modelos-codificacion/-/blob/master/Ejercicio79.py)<br/>
[81.Crear un módulo de funciones aritméticas que realicen las operaciones de suma, resta, multiplicación, división y potencia de enteros, escribir un programa que importe este módulo y use estas funciones para operar con números capturados desde el teclado.](https://gitlab.com/magordini/modelos-codificacion/-/blob/master/Ejercicio81/Ejercicio81.py)<br/>
[82.Crear un módulo que permita validar un número según la base en la cual este expresado, las bases validas deben ser 2, 8, 10 y 16.](https://gitlab.com/magordini/modelos-codificacion/-/blob/master/Ejercicio82/Ejercicio82.py)<br/>
[86.Escribir un programa que escriba la lista de caracteres ASCII dentro de un archivo de texto. ](https://gitlab.com/magordini/modelos-codificacion/-/blob/master/Ejercicio86&87/Ejercicio86.py)<br/>
[87.Escribir un programa que lea y muestre en pantalla el archivo generado en el ejercicio anterior.](https://gitlab.com/magordini/modelos-codificacion/-/blob/master/Ejercicio86&87/Ejercicio87.py)<br/>
[90.Escribir un programa que lea un tiempo en horas, minutos y segundos y empiece a cronometrar el tiempo mostrándolo en pantalla hasta llegar al limite leído al inicio.](https://gitlab.com/magordini/modelos-codificacion/-/blob/master/Ejercicio90.py)<br/>
[91.Modifique el ejercicio anterior para que el conteo se haga hacia atrás desde el tiempo leído hasta cero.](https://gitlab.com/magordini/modelos-codificacion/-/blob/master/Ejercicio91.py)